(function main() {
    document.getElementById('fadeInPlay')
        .addEventListener('click', function () {
            const block = document.getElementById('fadeInBlock');
            animaster().fadeIn(block, 5000);
        });

    document.getElementById('movePlay')
        .addEventListener('click', function () {
            const block = document.getElementById('moveBlock');
            animaster().move(block, 1000, {x: 100, y: 10});
        });

    document.getElementById('scalePlay')
        .addEventListener('click', function () {
            const block = document.getElementById('scaleBlock');
            animaster().scale(block, 1000, 1.25);
        });

    document.getElementById('fadeOutPlay')
        .addEventListener('click', function () {
            const block = document.getElementById('fadeOutBlock');
            animaster().fadeOut(block, 1000, 1.25);
        });



    document.getElementById('moveAndHidePlay')
        .addEventListener('click', function () {
            const block = document.getElementById('moveAndHideBlock');
            animaster().move(block, 2000, {x: 100, y: 20});
            animaster().fadeOut(block, 3000, 1.25);

        });

    document.getElementById('showAndHidePlay')
        .addEventListener('click', function () {
            const block = document.getElementById('showAndHideBlock');
            setInterval(animaster().fadeOut, 1000, block, 1000);
            setInterval(animaster().fadeIn, 2000, block, 1000);
        });

    document.getElementById('heartBeatingPlay')
        .addEventListener('click', function () {
            const block = document.getElementById('heartBeatingBlock');
            // animaster().scale(block, 1000, 1.4);
            setInterval(animaster().scale, 1000, block, 1000, 1.4);
            setInterval(animaster().scale, 2000, block, 1000, 1.0);
        });

    document.getElementById('shakingPlay')
        .addEventListener('click', function () {
            const block = document.getElementById('shakingBlock');
            setInterval(animaster().move, 250, block, 250, {x: 20, y: 0});
            setInterval(animaster().move, 500, block, 250, {x: -20, y: 0});
        });
})();


function animaster() {
    /**
     * Функция анимации блоков
     * @param element — HTMLElement, который надо анимировать
     * @param duration — Продолжительность анимации в миллисекундах
     * @param translation — объект с полями x и y, обозначающими смещение блока
     * @param ratio — во сколько раз увеличить/уменьшить. Чтобы уменьшить, нужно передать значение меньше 1
     */
    function fadeIn(element, duration) {
        element.style.transitionDuration = `${duration}ms`;
        element.classList.remove('hide');
        element.classList.add('show');
    }

    function move(element, duration, translation) {
        element.style.transitionDuration = `${duration}ms`;
        element.style.transform = getTransform(translation, null);
    }

    function scale(element, duration, ratio) {
        element.style.transitionDuration = `${duration}ms`;
        element.style.transform = getTransform(null, ratio);
    }

    function fadeOut(element, duration) {
        element.style.transitionDuration = `${duration}ms`;
        element.classList.remove('show');
        element.classList.add('hide');
    }

    let obj = {fadeIn, move, scale, fadeOut};

    return obj;
}

function getTransform(translation, ratio) {
    const result = [];
    if (translation) {
        result.push(`translate(${translation.x}px,${translation.y}px)`);
    }
    if (ratio) {
        result.push(`scale(${ratio})`);
    }
    return result.join(' ');
}

function moveAndHide(element, duration, translation) {
    element.style.transform = getTransform(translation, null);
}

function showAndHide(element, duration, translation) {
    element.style.transform = getTransform(translation, null);
}

function heartBeating(element, duration, translation) {
    element.style.transform = getTransform(translation, null);
}

function shaking(element, duration, translation) {
    element.style.transform = getTransform(translation, null);
}